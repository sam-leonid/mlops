build:
	docker build -t mlops_project:latest .

up:
	docker compose up -d

down:
	docker compose down

up_runner:
	docker run -d --name gitlab-runner --restart always \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v gitlab-runner-config:/etc/gitlab-runner \
	gitlab/gitlab-runner:latest

down_runner:
	docker stop gitlab-runner && docker rm gitlab-runner