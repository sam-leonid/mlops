# Install the pre-commit hooks:

```
run pre-commit install
```

# Linting with Ruff

We use Ruff as our primary linter. Ruff is a fast and easy-to-use linter that combines multiple linting tools into one. It helps maintain a consistent code style and catches potential errors.

To run Ruff manually, use the following command:

``` ruff check .```

Ruff will analyze your code and report any issues it finds. You can also use the --fix flag to automatically fix some of the issues:

# Formatting with Black

We use Black as our code formatter. Black is an opinionated formatter that enforces a consistent style across the project. It helps maintain readability and reduces formatting-related discussions during code reviews.

To run Black manually, use the following command:

```black .```

Black will format your code according to its predefined style. It will modify your files in-place.

# Pre-commit Hooks

To ensure that linting and formatting are performed automatically before each commit, we use pre-commit hooks. The pre-commit configuration is defined in the .pre-commit-config.yaml file.

The pre-commit hooks will run Ruff and Black on the staged files before each commit. If any issues are found, the commit will be rejected, and you will need to fix the issues before attempting to commit again.

To manually run the pre-commit hooks on all files, use the following command:

```pre-commit run --all-files```