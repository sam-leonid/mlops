# mlops

## Metodology

Metodology for this repo is Git Flow.
Git Flow is a branching model for Git that helps teams collaborate on software development projects.

Here's a brief overview of the Git Flow branches:

**main:** This is the main branch of the repository, where all the development work takes place. It's the branch that contains the latest version of the code that's being worked on.

**feature:** When a new feature is being developed, a new branch is created from main, and the feature is developed on this branch. Once the feature is complete, it's merged back into main.

**develop:** This is a branch that contains the latest development version of the product. It's used to develop new features and fixes, and to test them before they're released.

## how to run

```
make up
```

