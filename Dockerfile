FROM continuumio/miniconda3:24.1.2-0

WORKDIR /jup

COPY pyproject.toml \
     pdm.lock README.md \
     .pre-commit-config.yaml \
     notebooks ./

RUN conda install --yes -c  conda-forge pdm==2.14.0 \
                            jupyterlab==4.1.5 \
                            kaggle==1.6.12 \
                            pandas==2.2.1 \
                            matplotlib==3.8.4 \
                            folium==0.16.0

# install PDM
RUN pdm install --dev -v

# add PDM to env
RUN echo "source $(pdm info --packages)/bin/activate" >> ~/.bashrc
ENV PATH="/opt/conda/bin:/root/.local/bin:${PATH}"

EXPOSE 8888
